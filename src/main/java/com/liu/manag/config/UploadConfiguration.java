package com.liu.manag.config;

import com.liu.manag.utils.FileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class UploadConfiguration {

    public static String path;

    @Value("${upload.path}")
    public void setPath(String path){
        UploadConfiguration.path = path;

    }
    @PostConstruct
    public void init(){
        FileUtil.beforeCreateDirs(UploadConfiguration.path);
    }














}
