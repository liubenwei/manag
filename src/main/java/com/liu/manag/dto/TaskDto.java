package com.liu.manag.dto;

import lombok.Data;

import java.util.Date;

@Data
public class TaskDto {

    private Long id;
    private String name;
    private String status;
    private String assignTo;

    private String finishBy;

    private Integer expectHour;

    private Integer consumHour;
    private Integer surplusHour;
    private Date expectEndDate;

}
