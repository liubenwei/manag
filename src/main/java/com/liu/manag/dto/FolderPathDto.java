package com.liu.manag.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FolderPathDto {
    private Long id;

    private String fileName;
}
