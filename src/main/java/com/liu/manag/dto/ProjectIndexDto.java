package com.liu.manag.dto;

import lombok.Data;

import java.util.List;

@Data
public class ProjectIndexDto {

    private List<ProjectStatistDto> projectStatistDtoList;

    private ProjectOverviewDto projectOverviewDto;

    private List<ProjectDto> projectDtoList;
}
