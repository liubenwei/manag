package com.liu.manag.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class BugDto {

    private Long id;

    private String isConfirm;

    private String title;

    private String status;

    private String createBy;

    private LocalDateTime createDate;
    private String assignTo;

    private String finishPlan;
}
