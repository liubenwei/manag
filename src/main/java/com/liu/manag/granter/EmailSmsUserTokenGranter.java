package com.liu.manag.granter;

import com.liu.manag.entity.User;
import com.liu.manag.security.MyUserDetailsService;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

import java.util.Map;

public class EmailSmsUserTokenGranter extends AbstractUserTokenGranter{
    protected MyUserDetailsService userDetailsService;
    protected EmailSmsUserTokenGranter(MyUserDetailsService userDetailsService, AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, OAuth2RequestFactory requestFactory) {
        super(tokenServices, clientDetailsService, requestFactory, "sms");
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected User getUser(Map<String, String> parameters) {
        return null;
    }
}
