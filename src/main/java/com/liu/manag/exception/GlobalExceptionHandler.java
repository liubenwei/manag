package com.liu.manag.exception;

import com.liu.manag.utils.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = ClientException.class)
    public Result<String> ClientErrorHandler(HttpServletRequest req, ClientException e) {
        Result<String> result = new Result<>();
        result.setCode(400);
        result.setMessage(e.getMessage());
        result.setData("请求出错");
        return result;
    }
    @ExceptionHandler(value = ServerException.class)
    public Result<String> ServerErrorHandler(HttpServletRequest req, ServerException e) {
        Result<String> result = new Result<>();
        result.setCode(500);
        result.setStatus(400);
        result.setMessage(e.getMessage());
        result.setData("服务器内部出错");
        return result;
    }

    @ExceptionHandler(value = Exception.class)
    public Result<String>ErrorHandler(HttpServletRequest req, ServerException e) {
        Result<String> result = new Result<>();
        result.setStatus(500);
        result.setCode(500);
        result.setMessage(e.getMessage());
        result.setData("服务器内部出错");
        return result;
    }

}
