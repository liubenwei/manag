//package com.liu.manag.exception;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.boot.web.servlet.error.ErrorController;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//@Slf4j
//@RestController
//public class HttpErrorController  implements ErrorController {
//    private final String ERROR_PATH = "/error";
//
//    @Override
//    public String getErrorPath() {
//        return ERROR_PATH;
//    }
//    @RequestMapping("/error")
//    public String error(){
//        return "服务器出错";
//    }
//}
