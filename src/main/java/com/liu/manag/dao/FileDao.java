package com.liu.manag.dao;

import com.liu.manag.entity.File;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (File)表数据库访问层
 *
 * @author liu
 * @since 2020-03-19 11:51:00
 */
public interface FileDao {

    List<File> findByFolderId(@Param("folderId") Long folderId,@Param("userId")Long userId);

    File getByMd5(@Param("md5") String md5);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    File queryById(@Param("id") Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<File> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param file 实例对象
     * @return 对象列表
     */
    List<File> queryAll(File file);

    /**
     * 新增数据
     *
     * @param file 实例对象
     * @return 影响行数
     */
    int insert(@Param("file") File file);

    /**
     * 修改数据
     *
     * @param file 实例对象
     * @return 影响行数
     */
    int update(File file);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}