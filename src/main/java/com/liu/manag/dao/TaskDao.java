package com.liu.manag.dao;

import com.liu.manag.entity.Task;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Task)表数据库访问层
 *
 * @author liu
 * @since 2020-03-14 08:20:51
 */
public interface TaskDao {

    List<Task> getByPorjectId(@Param("projectId") Long projectId,@Param("status")Integer status,@Param("assignTo") Long assignTo,@Param("finishBy")Long finishBy,@Param("createBy") Long createBy,@Param("page") int page,@Param("size") int size);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Task queryById(@Param("id") Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Task> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param task 实例对象
     * @return 对象列表
     */
    List<Task> queryAll(Task task);

    /**
     * 新增数据
     *
     * @param task 实例对象
     * @return 影响行数
     */
    int insert(@Param("task") Task task);

    /**
     * 修改数据
     *
     * @param task 实例对象
     * @return 影响行数
     */
    int update(@Param("task") Task task);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}