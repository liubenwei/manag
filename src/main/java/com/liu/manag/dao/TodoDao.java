package com.liu.manag.dao;

import com.liu.manag.entity.Todo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Todo)表数据库访问层
 *
 * @author liu
 * @since 2020-03-11 10:09:10
 */
public interface TodoDao {

    List<Todo> queryByUserId(@Param("userId") Long userId);

    List<Todo> queryByIds(@Param("userId") Long userId, @Param("ids") Long[] ids);

    List<Todo> queryByStatus(@Param("userId") Long userId, @Param("status") int status, @Param("page") int page, @Param("size") int size);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Todo queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Todo> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param todo 实例对象
     * @return 对象列表
     */
    List<Todo> queryAll(Todo todo);

    /**
     * 新增数据
     *
     * @param todo 实例对象
     * @return 影响行数
     */
    int insert(@Param("todo") Todo todo);

    /**
     * 修改数据
     *
     * @param todo 实例对象
     * @return 影响行数
     */
    int update(@Param("todo") Todo todo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long userId, Long id);

}