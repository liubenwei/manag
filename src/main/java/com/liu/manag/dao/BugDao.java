package com.liu.manag.dao;

import com.liu.manag.entity.Bug;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Bug)表数据库访问层
 *
 * @author liu
 * @since 2020-03-19 10:28:08
 */
public interface BugDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Bug queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Bug> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param bug 实例对象
     * @return 对象列表
     */
    List<Bug> queryAll(Bug bug);

    /**
     * 新增数据
     *
     * @param bug 实例对象
     * @return 影响行数
     */
    int insert(@Param("bug") Bug bug);

    /**
     * 修改数据
     *
     * @param bug 实例对象
     * @return 影响行数
     */
    int update(Bug bug);


    void updateBug(@Param("bug")Bug bug);
    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);


    List<Bug> getBug(@Param("bug") Bug bug,@Param("page") Integer page, @Param("size") Integer size);
}