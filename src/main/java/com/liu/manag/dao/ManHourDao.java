package com.liu.manag.dao;

import com.liu.manag.entity.ManHour;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (ManHour)表数据库访问层
 *
 * @author liu
 * @since 2020-03-20 10:09:47
 */
public interface ManHourDao {

    List<ManHour> queryByTaskId(@Param("taskId")Long taskId);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ManHour queryById(Long id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ManHour> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param manHour 实例对象
     * @return 对象列表
     */
    List<ManHour> queryAll(ManHour manHour);

    /**
     * 新增数据
     *
     * @param manHour 实例对象
     * @return 影响行数
     */
    int insert(ManHour manHour);

    /**
     * 修改数据
     *
     * @param manHour 实例对象
     * @return 影响行数
     */
    int update(ManHour manHour);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);

}