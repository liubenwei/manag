package com.liu.manag;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.stream.IntStream;

@SpringBootApplication
@MapperScan("com.liu.manag.dao")
public class ManagApplication {

    public static void main(String[] args) {
//        System.out.println(IntStream.rangeClosed(1,100).filter(i -> i%2 == 0).map(i ->  i*i).sum());;
        SpringApplication.run(ManagApplication.class, args);
    }

}
