package com.liu.manag.enums;

public enum StatusEnum {
    NOT_BEGIN(0, "新建"),
    RUNNING(1, "进行中"),
    FINISHED(2, "已完成"),
    CLOSED(3, "已关闭"),
    DELETED(-1, "已删除");
    private int status;
    private String value;

    StatusEnum(int status, String value) {
        this.status = status;
        this.value = value;
    }

    public int getStatus() {
        return status;
    }

    public String getValue() {
        return value;
    }

    public static String getValueByKey(int key) {
        for (StatusEnum type : StatusEnum.values()) {
            if (type.getStatus() == key) {
                return type.getValue();
            }
        }
        return "";
    }

    public static int getKeyByValue(String value) {
        for (StatusEnum type : StatusEnum.values()) {
            if (type.getValue().equalsIgnoreCase(value)) {
                return type.getStatus();
            }
        }
        return -2;
    }
}
