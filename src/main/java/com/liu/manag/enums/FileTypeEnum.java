package com.liu.manag.enums;

public enum FileTypeEnum {
    /**
     * 文件类型
     */
    DEFAULT,
    FOLDER,
    VIDEO,
    AUDIO,
    PDF,
    COMPRESS_FILE,
    PICTURE,
    DOC,
    PPT,
    TXT,
    TORRENT,
    WEB,
    CODE
    ;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
