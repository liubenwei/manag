package com.liu.manag.enums;

public enum TypeEnum {
    PROJECT(0,"项目"),
    TASK(1, "任务"),
    BUG(2, "bug"),
    DEMAND(3, "需求"),
    CUSTOMIZE(4, "自定义");
    private int type;
    private String value;

    TypeEnum(int type) {
        this.type = type;
    }

    TypeEnum(int type, String value) {
        this.type = type;
        this.value = value;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    //根据key获取value
    public static String getValueByKey(int key){
        for(TypeEnum type : TypeEnum.values()){
            if(type.getType() == key){
                return type.getValue();
            }
        }
        return "";
    }

    public static int getKeyByValue(String value){
        for(TypeEnum type : TypeEnum.values()){
            if(type.getValue().equalsIgnoreCase(value)){
                return type.getType();
            }
        }
        return -2;
    }
}
