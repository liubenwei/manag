package com.liu.manag.enums;

public enum TaskTypeEnum {
    DEVLOP(0, "开发"),
    DESIGN(1, "设计"),
    TEST(2, "测试"),
    DISCUSS(3, "讨论"),
    OTHER(4, "其他");
    private Integer type;
    private String value;

    TaskTypeEnum(int type, String value) {
        this.type = type;
        this.value = value;
    }

    public static String getValueByKey(int key) {
        for (StatusEnum type : StatusEnum.values()) {
            if (type.getStatus() == key) {
                return type.getValue();
            }
        }
        return "";
    }

    public static int getKeyByValue(String value) {
        for (StatusEnum type : StatusEnum.values()) {
            if (type.getValue().equalsIgnoreCase(value)) {
                return type.getStatus();
            }
        }
        return -2;
    }
}
