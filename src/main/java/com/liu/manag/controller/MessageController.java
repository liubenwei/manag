package com.liu.manag.controller;

import com.liu.manag.entity.Message;
import com.liu.manag.entity.User;
import com.liu.manag.security.UserAuthenticationExtractor;
import com.liu.manag.service.MessageService;
import com.liu.manag.service.UserService;
import com.liu.manag.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.List;

/**
 * (Message)表控制层
 *
 * @author liu
 * @since 2020-03-19 20:37:30
 */
@RestController
@RequestMapping("/message")
public class MessageController {

    /**
     * 服务对象
     */
    @Resource
    private MessageService messageService;


    @Autowired
    private UserService userService;

    /**
     *
     */
    @GetMapping("/{id}")
    public Result<User> getUserById(@PathVariable("id") Long id) {
        User user = userService.queryById(id);
        return new Result<>(user);
    }

    @PostMapping("/push/{toId}")
    public Result<String> pushMessage1(@PathVariable("toId") Long toId,
                                      @RequestParam("content") String content,
                                      Principal principal) {
        User operator = UserAuthenticationExtractor.extract(principal);
        try {
            WebsocketServerEndpoint endpoint = new WebsocketServerEndpoint();
            endpoint.sendTo(operator.getId(), toId, content);
            return new Result<>("推送成功");
        } catch (Exception e) {
            e.printStackTrace();
            Result<String> result = new Result<>("推送失败");
            result.setCode(500);
            result.setStatus(500);
            return  result;
        }
    }


    @PostMapping("/push")
    public Result<String> pushMessage(@RequestParam("toId") Long toId,
                                      @RequestParam("content") String content,
                                      Principal principal) {
        User operator = UserAuthenticationExtractor.extract(principal);


        try {
            WebsocketServerEndpoint endpoint = new WebsocketServerEndpoint();
            endpoint.sendTo(operator.getId(), toId, content);
            return new Result<>("推送成功");
        } catch (Exception e) {
            e.printStackTrace();
            Result<String> result = new Result<>("推送失败");
            result.setCode(500);
            result.setStatus(500);
            return  result;
        }
    }

    @GetMapping("/online/list")
    public Result<List<User>> getOnlineList(){
        List<User> users =  userService.queryAllByLimit(0,100);
        return new Result<>(users);
    }
    @GetMapping("/self/{fromId}/{toId}")
    public Result<List<Message>> selfList(@PathVariable("fromId") Long fromId, @PathVariable("toId") Long toId) {
        String conversationId = fromId < toId ? String.format("%d_%d", fromId, toId) : String.format("%d_%d", toId, fromId);

        List<Message> list = messageService.findByConversationId(conversationId);
        return new Result<>(list);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Message selectOne(Long id) {
        return this.messageService.queryById(id);
    }

}