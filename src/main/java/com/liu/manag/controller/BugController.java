package com.liu.manag.controller;

import com.liu.manag.dto.BugDto;
import com.liu.manag.entity.Bug;
import com.liu.manag.entity.User;
import com.liu.manag.enums.StatusEnum;
import com.liu.manag.security.UserAuthenticationExtractor;
import com.liu.manag.service.BugService;
import com.liu.manag.service.UserService;
import com.liu.manag.utils.Result;
import org.bouncycastle.cert.ocsp.Req;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * (Bug)表控制层
 *
 * @author liu
 * @since 2020-03-19 10:28:08
 */
@RestController
@RequestMapping("/bug")
public class BugController {
    /**
     * 服务对象
     */
    @Resource
    private BugService bugService;

    @Autowired
    private UserService userService;


    @GetMapping("/page")
    public Result<List<BugDto>> getBug(@RequestParam("projectId") Long projectId,
                                    @RequestParam(value = "status", required = false) Integer status,
                                    @RequestParam(value = "assignTo", required = false) Long assignTo,
                                    @RequestParam(value = "finishBy", required = false) Long finishBy,
                                    @RequestParam(value = "createBy", required = false) Long createBy,
                                    @RequestParam("page") Integer _page,
                                    @RequestParam("size") Integer _size,
                                    Principal principal) {

        User operator = UserAuthenticationExtractor.extract(principal);
        int page = Objects.nonNull(_page) ? _page - 1 : 0;
        int size = Objects.nonNull(_size) ? _size : 10;
        List<Bug> bugList = bugService.getBug(projectId, status, assignTo, finishBy, createBy, operator, page, size);
        List<BugDto> bugDtos = new ArrayList<>();
        for(Bug bug : bugList){
            BugDto bugDto = new BugDto();
            bugDto.setId(bug.getId());
            bugDto.setAssignTo(userService.queryById(bug.getAssignTo()).getUsername());
            bugDto.setFinishPlan(bug.getFinishPlan());
            bugDto.setCreateBy(userService.queryById(bug.getCreateBy()).getUsername());
            if(bug.getIsConfirm() == 0 ){
                bugDto.setIsConfirm("否");
            }else{
                bugDto.setIsConfirm("是");
            }
            bugDto.setStatus(StatusEnum.getValueByKey(bug.getStatus()));
            bugDto.setTitle(bug.getTitle());
            bugDto.setCreateDate(bug.getCreateTime());
            bugDtos.add(bugDto);


        }

        return new Result<>(bugDtos);


    }

    @PutMapping("")
    public Result<String> updateBug(@RequestParam("title") String title,
                                    @RequestParam("describe") String describe,
                                    @RequestParam("remark") String remark,
                                    @RequestParam("assignTo") Long assignTo,
                                    @RequestParam("bugType") Integer bugType,
                                    @RequestParam("bugId") Long bugId,
                                    @RequestParam("status") Integer status,
                                    @RequestParam("confirmBy")Long confirmBy,
                                    Principal principal) {

        User operator = UserAuthenticationExtractor.extract(principal);
        bugService.updateBug(bugId, title, describe, remark, assignTo, bugType, status, operator.getId(), operator,confirmBy);

        return new Result<>("更新成功");
    }

    public Result<String> deleteBug(@RequestParam("bugId") Long bugId,
                                    Principal principal) {
        User operator = UserAuthenticationExtractor.extract(principal);
        bugService.deleteById(bugId,operator);
        return new Result<>("删除成功");
    }


    @PostMapping("")
    public Result<String> addBug(@RequestParam("projectId") Long projectId,
                                 @RequestParam("version") String version,
                                 @RequestParam("assignTo") Long assignTo,
                                 @RequestParam("bugType") Integer bugType,
                                 @RequestParam("title") String title,
                                 @RequestParam("describe") String describe,
                                 Principal principal) {

        User operator = UserAuthenticationExtractor.extract(principal);

        bugService.addBug(projectId, version, assignTo, bugType, title, describe, operator.getId());
        return new Result<>("添加成功");
    }


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Bug selectOne(Long id) {
        return this.bugService.queryById(id);
    }

}