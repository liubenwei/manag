package com.liu.manag.controller;

import com.liu.manag.dto.TaskDto;
import com.liu.manag.entity.ManHour;
import com.liu.manag.entity.Task;
import com.liu.manag.entity.User;
import com.liu.manag.enums.StatusEnum;
import com.liu.manag.enums.TaskTypeEnum;
import com.liu.manag.security.UserAuthenticationExtractor;
import com.liu.manag.service.ManHourService;
import com.liu.manag.service.TaskService;
import com.liu.manag.service.UserService;
import com.liu.manag.utils.Result;
import com.liu.manag.vo.TaskVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * (Task)表控制层
 *
 * @author liu
 * @since 2020-03-14 08:20:52
 */
@RestController
@RequestMapping("task")
public class TaskController {
    /**
     * 服务对象
     */
    @Resource
    private TaskService taskService;

    @Autowired
    private UserService userService;

    @Autowired
    private ManHourService manHourService;


    @GetMapping("")
    public Result<Task> getTaskDetail(@RequestParam("taskId")Long taskId){
        return new Result<>(taskService.queryById(taskId));
    }

    @GetMapping("/page")
    public Result<List<TaskDto>> getTask(@RequestParam("projectId")Long projectId,
                                        @RequestParam(value = "status",required = false) Integer status,
                                        @RequestParam(value = "assignTo",required = false)Long assignTo,
                                        @RequestParam(value = "finishBy",required = false)Long finishBy,
                                        @RequestParam(value = "createBy",required = false)Long createBy,
                                        @RequestParam("page")Integer _page,
                                        @RequestParam("size") Integer _size) {

        int page = Objects.nonNull(_page) ? _page - 1 : 0;
        int size = Objects.nonNull(_size) ? _size : 10;
        List<Task> tasks = taskService.getByProjecId(projectId, status, assignTo, finishBy, createBy, page, size);
        List<TaskDto> taskDtos = new ArrayList<>();
        for(Task task : tasks){
            TaskDto taskDto =new TaskDto();

            taskDto.setId(task.getId());
            taskDto.setExpectHour(task.getExpectHour());
            taskDto.setStatus(StatusEnum.getValueByKey(task.getStatus()));
            taskDto.setName(task.getName());
            taskDto.setExpectEndDate(task.getExpectEndDate());
            taskDto.setAssignTo(userService.queryById(task.getAssginTo()).getUsername());
            taskDto.setFinishBy(userService.queryById(task.getFinishBy()).getUsername());
            taskDto.setConsumHour(task.getConsumHour());
            taskDto.setSurplusHour(task.getSurplusHour());
            taskDtos.add(taskDto);
        }
        return new Result<>(taskDtos);
    }



    @PutMapping("")
    public Result<String> updateTask(@RequestParam("name") String name,
                                     @RequestParam("describe") String describe,
                                     @RequestParam("remark") String remark,
                                     @RequestParam("assignTo") Long assignTo,
                                     @RequestParam("taskType") Integer taskType,
                                     @RequestParam("taskId") Long taskId,
                                     @RequestParam("status")Integer status,
                                     Principal principal
    ) {
        User operator = UserAuthenticationExtractor.extract(principal);
        taskService.updateTask(taskId,status, name, describe, remark, assignTo, taskType, operator.getId(),operator.getUsername());

        return new Result<>("修改成功");
    }

    @PostMapping("")
    public Result<String> addTask(@RequestParam("projectId") Long projectId,
                                  @RequestParam("taskType") Integer taskType,
                                  @RequestParam("assignTo") Long assignTo,
                                  @RequestParam("name") String name,
                                  @RequestParam(value = "describe", required = false) String describe,
                                  @RequestParam(value = "expectBeginDate", required = false) Date expectBeginDate,
                                  @RequestParam(value = "expectEndDate", required = false) Date expectEndDate,
                                  @RequestParam(value = "expectHour", required = false) Integer expectHour,
                                  Principal principal) {

        User operator = UserAuthenticationExtractor.extract(principal);
        taskService.addTask(projectId, taskType, assignTo, name, describe, expectBeginDate, expectBeginDate, expectHour, operator.getId(), operator.getUsername());
        return new Result<>("添加成功");
    }

    @DeleteMapping("")
    public Result<String> deleteTask(@RequestParam("taskId") Long taskId) {
        taskService.deleteById(taskId);
        return new Result<>("添加成功");

    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Task selectOne(Long id) {
        return this.taskService.queryById(id);
    }

}