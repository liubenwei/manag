package com.liu.manag.controller;

import com.liu.manag.entity.Demand;
import com.liu.manag.service.DemandService;
import com.liu.manag.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Demand)表控制层
 *
 * @author liu
 * @since 2020-03-14 08:21:11
 */
@RestController
@RequestMapping("demand")
public class DemandController {
    /**
     * 服务对象
     */
    @Resource
    private DemandService demandService;

    @PostMapping("")
    public Result<String> addDemand(@RequestParam("projectId")Long projectId,
                                    @RequestParam("name")String name,
                                    @RequestParam("describe")String describe){
        return null;
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Demand selectOne(Long id) {
        return this.demandService.queryById(id);
    }

}