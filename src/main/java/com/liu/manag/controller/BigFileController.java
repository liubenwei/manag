package com.liu.manag.controller;

import com.liu.manag.entity.File;
import com.liu.manag.entity.User;
import com.liu.manag.security.UserAuthenticationExtractor;
import com.liu.manag.service.FileService;
import com.liu.manag.utils.Result;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

/**
 * (File)表控制层
 *
 * @author liu
 * @since 2020-03-19 11:51:00
 */
@RestController
@RequestMapping("/BigFile")
public class BigFileController {
    /**
     * 服务对象
     */
    @Resource
    private FileService fileService;



    @PostMapping("")
    public void uploadFile(@RequestParam("name") String name,
                           @RequestParam("md5")String md5,
                           @RequestParam("size")Long size,
                           @RequestParam("chunks")Integer chunks,
                           @RequestParam("chunk")Integer chunk,
                           @RequestParam(value = "parentId",defaultValue = "1")Long parentId,
                           @RequestParam("file")MultipartFile file,
                           Principal principal) throws IOException {

//        User operator = UserAuthenticationExtractor.extract(principal);
        if(chunks != null && chunks != 0){
            fileService.uploadWithBlock(name,md5,size,chunks,chunk,file);
        }else{
            fileService.upload(name,md5,parentId,file,1L);
        }
    }


    @GetMapping("")
    public Result<List<File>> getFiles(){
        return null;
    }

    @GetMapping("/{fileId}")
    public Result<List<File>> getFilesByFileId(){
        return null;
    }

    /**
     * 修改文件名
     * @return
     */
    @PutMapping("")
    public Result<String> updateFile(){
        return null;
    }

    @GetMapping("/download")
    public void download(){

    }
    public Result<String> deleteFile(){
        return null;
    }
    @PostMapping("/md5")
    public Result<Boolean> checkMd5(@RequestParam("md5") String md5){
        Boolean isUpload = fileService.checkMd5(md5);
        return new Result<>(isUpload);
    }


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public File selectOne(Long id) {
        return this.fileService.queryById(id);
    }

}