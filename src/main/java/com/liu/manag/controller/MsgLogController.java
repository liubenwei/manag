package com.liu.manag.controller;

import com.liu.manag.entity.MsgLog;
import com.liu.manag.service.MsgLogService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 消息投递日志(MsgLog)表控制层
 *
 * @author liu
 * @since 2020-03-02 17:42:21
 */
@RestController
@RequestMapping("msgLog")
public class MsgLogController {
    /**
     * 服务对象
     */
    @Resource
    private MsgLogService msgLogService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public MsgLog selectOne(String id) {
        return this.msgLogService.queryById(id);
    }

}