package com.liu.manag.controller;

import com.liu.manag.entity.ManHour;
import com.liu.manag.entity.User;
import com.liu.manag.security.UserAuthenticationExtractor;
import com.liu.manag.service.ManHourService;
import com.liu.manag.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * (ManHour)表控制层
 *
 * @author liu
 * @since 2020-03-20 10:09:47
 */
@RestController
@RequestMapping("manHour")
public class ManHourController {
    /**
     * 服务对象
     */
    @Resource
    private ManHourService manHourService;

    @PostMapping("")
    public Result<String> addManHour(@RequestParam("taskId") Long taskId,
                                     @RequestParam("consumHour") Integer consumHour,
                                     @RequestParam("surplusHour") Integer surplusHour,
                                     @RequestParam("remark") String remark,
                                     @RequestParam("date") Date date,
                                     Principal principal) {

        User operator = UserAuthenticationExtractor.extract(principal);
        manHourService.addManHour(taskId, consumHour, surplusHour, remark, date, operator.getId(),operator.getUsername());
        return new Result<>("添加成功");
    }

    @GetMapping("")
    public Result<List<ManHour>> getManHOUR(@RequestParam("taskId")Long  taskId) {
            return new Result<>(manHourService.getByTaskId(taskId));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public ManHour selectOne(Long id) {
        return this.manHourService.queryById(id);
    }

}