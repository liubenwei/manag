package com.liu.manag.controller;

import com.liu.manag.dto.ProjectDto;
import com.liu.manag.dto.ProjectIndexDto;
import com.liu.manag.entity.Project;
import com.liu.manag.entity.User;
import com.liu.manag.security.UserAuthenticationExtractor;
import com.liu.manag.service.ProjectService;
import com.liu.manag.utils.Result;
import com.liu.manag.vo.ProjectListVo;
import org.apache.ibatis.annotations.ResultType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * (Project)表控制层
 *
 * @author liu
 * @since 2020-03-14 08:20:32
 */
@RestController
@RequestMapping("/project")
public class ProjectController {
    /**
     * 服务对象
     */
    @Resource
    private ProjectService projectService;




    @GetMapping("")
    public Result<List<ProjectIndexDto>> getProject(){

        return null;
    }


    @PreAuthorize("hasRole('admin')")
    public Result<String> deleteProject(@RequestParam("projectId") Long projectId) {
        projectService.deleteById(projectId);
        return new Result<>("删除成功");

    }

    /**
     * useGeneratedKeys="true" keyProperty="id"
     *
     * @param projectName
     * @param beginDate
     * @param endDate
     * @param usableDays
     * @param describe
     * @return
     */

    @PreAuthorize("hasRole('admin')")
    @PostMapping("")
    public Result<String> addProject(@RequestParam("projectName") String projectName,
                                     @RequestParam("beginDate") Date beginDate,
                                     @RequestParam("endDate") Date endDate,
                                     @RequestParam(value = "usableDays", required = false) Integer usableDays,
                                     @RequestParam(value = "describe", required = false) String describe,
                                     Principal principal) {

        User operator = UserAuthenticationExtractor.extract(principal);
        Long projectId = projectService.insertProject(projectName, beginDate, endDate, usableDays, describe, operator.getId(), operator.getUsername());


        return new Result<>("添加成功");
    }

    @GetMapping("/all")
    public Result<List<ProjectListVo>> getAllProject() {
        List<Project> projectList = projectService.queryAllByLimit(0, 200);
        ProjectListVo projectListVo = new ProjectListVo();
        List<ProjectListVo> projectListVos = projectList.stream().map(project -> {
            ProjectListVo vo = new ProjectListVo();
            vo.setPeojectId(project.getId());
            vo.setProjectName(project.getName());
            return vo;
        }).collect(Collectors.toList());
        return new Result<>(projectListVos);
    }


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Project selectOne(Long id) {
        return this.projectService.queryById(id);
    }

}