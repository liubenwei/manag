package com.liu.manag.controller;

import com.liu.manag.dto.UserDto;
import com.liu.manag.entity.User;
import com.liu.manag.pojo.Mail;
import com.liu.manag.service.RabbitmqService;
import com.liu.manag.service.UserService;
import com.liu.manag.utils.Result;
import com.liu.manag.utils.StringRedisClient;
import org.redisson.RedissonSubList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * (User)表控制层
 *
 * @since 2020-03-01 19:59:19
 */
@RestController
@RequestMapping("/user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    @Autowired
    private RabbitmqService rabbitmqService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private StringRedisClient stringRedisClient;

    @GetMapping("/all")
    public Result<List<UserDto>> getAllUser() {
        List<User> users = userService.queryAllByLimit(0, 100);
        List<UserDto> userDtos = new ArrayList<>();
        for (User user : users) {
            UserDto userDto = new UserDto();
            userDto.setId(user.getId());
            userDto.setUsername(user.getUsername());
            userDtos.add(userDto);
        }
        return new Result<>(userDtos);
    }


    @PostMapping("/mail")
    public Result<String> sendMail(@RequestParam("to") String to) {

        //todo 验证重名
        Mail mail = new Mail();
        mail.setTo(to.toString());
        mail.setTitle("注册验证码");
        Integer code = new Random().nextInt(899999) + 10000;
        mail.setContent("您的注册验证码为:" + code);
        rabbitmqService.send(mail);

        return Result.sucess("发送成功");
    }

    @PostMapping("/regist")
    public Result<String> register(@RequestParam(value = "username") String username,
                                   @RequestParam(value = "password") String password,
                                   @RequestParam(value = "email") String email,
                                   @RequestParam(value = "verifyCode") String verifyCode,
                                   @RequestParam(value = "realname", required = false) String realname) {
        // todo union all  添加rabbitmq 更新

        String code = stringRedisClient.get(email);
        if (!code.equalsIgnoreCase(verifyCode)) {
            return new Result<>(400, 400, "验证码错误");
        }

        User user = new User();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setEmail(email);
        user.setRealName(realname);
        userService.insert(user);
        return new Result<>("注册成功");

    }


    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @PreAuthorize("hasRole('admin')")
    @GetMapping("/selectOne")
    public User selectOne(Long id, Principal principal) {

        System.out.println(principal.getName());
        return this.userService.queryById(id);
    }

}