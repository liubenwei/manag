package com.liu.manag.entity;

import lombok.Data;

import java.util.Date;
import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * (Project)实体类
 *
 * @author makejava
 * @since 2020-03-14 08:20:32
 */
@Data
public class Project implements Serializable {
    private static final long serialVersionUID = 489644405302152848L;


    private Long id;
    /**
    * 项目名称
    */
    private String name;
    /**
    * 起始日期
    */
    private Date beginDate;
    /**
    * 截止日期
    */
    private Date endDate;
    /**
    * 可用工作日
    */
    private Integer usableDays;
    /**
    * 描述
    */
    private String describe;
    /**
    * 创建人
    */
    private Long createBy;
    /**
    * 创建时间
    */
    private LocalDateTime createTime;
    /**
    * 关闭人
    */
    private Long closeBy;
    /**
    * 关闭时间
    */
    private LocalDateTime closeTime;
    /**
    * 项目负责人
    */
    private Long projectPrincipal;
    /**
    * 测试负责人
    */
    private Long testPrincipal;
    /**
    * 发布负责人
    */
    private Long releasePrincipal;


}