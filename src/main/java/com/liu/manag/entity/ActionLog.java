package com.liu.manag.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * (ActionLog)实体类
 *
 * @author makejava
 * @since 2020-03-16 22:00:27
 */
@Data
public class ActionLog implements Serializable {
    private static final long serialVersionUID = -85982669295518261L;

    private String entityName;
    private Long id;
    /**
    * 用户名
    */
    private Long userId;
    /**
    * 用户名
    */
    private String username;
    /**
    * 操作
    */
    private String action;
    /**
    * ip
    */
    private String ip;
    /**
    * 操作时间
    */
    private LocalDateTime actionTime;
    /**
    * 0、登陆1、项目 2、任务 3、bug 4、需求
    */
    private Integer type;
    /**
    * 实体id
    */
    private Long entityId;


}