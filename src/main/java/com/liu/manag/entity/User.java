package com.liu.manag.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * (User)实体类
 *
 * @author liu
 * @since 2020-03-01 19:59:19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable, UserDetails {
    private static final long serialVersionUID = -49016735937346245L;
    
    private Long id;
    
    private String realName;
    
    private String username;
    
    private String password;
    /**
    * 职位：1、项目经理 2、研发 3、测试 3、产品
    */
    private Integer position;
    /**
    * 邮箱
    */
    private String email;
    
    private String sex;
    
    private String phone;
    /**
    * 入职时间
    */
    private LocalDateTime createTime;
    /**
    * 最后登录
    */
    private LocalDateTime lastLogin;
    /**
    * 0、正常 -1 离职
    */
    private Integer state;
    /**
    * 访问次数
    */
    private Integer vistis;

    private Collection<? extends GrantedAuthority> authorities;


    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }
}