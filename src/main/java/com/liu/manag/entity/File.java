package com.liu.manag.entity;

import com.liu.manag.enums.FileTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * (File)实体类
 *
 * @author makejava
 * @since 2020-03-23 13:57:19
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class File implements Serializable {
    private static final long serialVersionUID = -83170490398233781L;

    public static final File ROOT_FILE = File.builder()
            .id(0L).fileName("全部文件").fileType(FileTypeEnum.FOLDER.toString()).parentId(0L).build();

    private Long updateBy;

    private LocalDateTime updateTime;
    private Long id;
    /**
    * 文件名
    */
    private String fileName;
    /**
    * 文件大小
    */
    private Object fileSize;
    /**
    * 父目录id
    */
    private Long parentId;

    private String fileType;
    /**
    * 真实目录
    */
    private String realPath;

    private Integer isShare;
    
    private Long createBy;
    
    private LocalDateTime createTime;
    /**
    * 文件的md5
    */
    private String md5;


}