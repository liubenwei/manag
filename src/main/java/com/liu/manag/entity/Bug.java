package com.liu.manag.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * (Bug)实体类
 *
 * @author makejava
 * @since 2020-03-19 10:57:30
 */
@Data
public class Bug implements Serializable {
    private static final long serialVersionUID = 781174868697893845L;
    
    private Long id;
    
    private Long projectId;
    /**
    * 影响版本
    */
    private String version;
    /**
    * 指派人姓名
    */
    private String assignToName;
    /**
    * 指派给
    */
    private Long assignTo;
    /**
    * 指派时间
    */
    private LocalDateTime assignTime;
    /**
    * bug类型
    */
    private Integer bugType;
    /**
    * bug标题
    */
    private String title;
    /**
    * bug描述
    */
    private String describe;
    /**
    * 确认人
    */
    private Long confirmBy;
    /**
    * 确认时间
    */
    private LocalDateTime confirmTime;
    
    private Long closeBy;
    /**
    * 关闭时间
    */
    private LocalDateTime closeTime;
    
    private Long finishBy;
    /**
    * 完成时间
    */
    private LocalDateTime finishTime;
    /**
    * 0、未确认 1、已确认
    */
    private Integer isConfirm;
    /**
    * 状态
    */
    private Integer status;
    /**
    * 解决方案
    */
    private String finishPlan;
    /**
    * 备注
    */
    private String remark;
    
    private Long createBy;
    
    private LocalDateTime createTime;


}