package com.liu.manag.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (Demand)实体类
 *
 * @author makejava
 * @since 2020-03-14 08:21:11
 */
@Data
public class Demand implements Serializable {
    private static final long serialVersionUID = 854930659197693405L;
    
    private Long id;
    /**
    * 项目id
    */
    private Long projectId;
    /**
    * 需求名称
    */
    private String name;


}