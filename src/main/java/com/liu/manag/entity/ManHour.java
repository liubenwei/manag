package com.liu.manag.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

/**
 * (ManHour)实体类
 *
 * @author makejava
 * @since 2020-03-20 10:09:47
 */
@Data
public class ManHour implements Serializable {
    private static final long serialVersionUID = -68628272793506058L;

    private Date date;
    private Integer surplusHour;
    private Long id;
    
    private Long taskId;
    
    private LocalDateTime beginTime;
    
    private Integer consumHour;
    /**
    * 备注
    */
    private String mark;
    
    private Long createBy;
    
    private LocalDateTime createTime;


}