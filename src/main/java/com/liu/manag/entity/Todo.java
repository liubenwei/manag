package com.liu.manag.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * (Todo)实体类
 *
 * @author liu
 * @since 2020-03-11 10:09:10
 */
@Data
public class Todo implements Serializable {
    private static final long serialVersionUID = -72767880878573937L;
    
    private Long id;
    /**
    * 日期
    */
    private Date date;
    /**
    * 1、bug 2、任务 3、需求 4 、自定义
    */
    private Integer type;
    /**
    * 待办名称
    */
    private String name;
    /**
    * 描述
    */
    private String describe;
    /**
    * 状态 0、未开始 1、进行中 2、已完成  3、已关闭 -1 已删除
    */
    private Integer state;
    /**
    * 创建者
    */
    private Long userId;
    /**
    * 开始时间
    */
    private Integer begin;
    /**
    * 结束时间
    */
    private Integer end;


}