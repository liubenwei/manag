package com.liu.manag.entity;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * (Message)实体类
 *
 * @author makejava
 * @since 2020-03-19 20:37:30
 */
@Data
public class Message implements Serializable {
    private static final long serialVersionUID = 634759336467679539L;
    
    private Long id;
    /**
    * 发送方
    */
    private Long fromId;
    /**
    * 接收方
    */
    private Long toId;
    /**
    * 内容
    */
    private String content;
    /**
    * 0、未读 1、已读
    */
    private Integer hasRead;
    
    private String conversationId;

    private LocalDateTime createTime;


}