package com.liu.manag.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

/**
 * (Task)实体类
 *
 * @author makejava
 * @since 2020-03-14 08:20:51
 */
@Data
public class Task implements Serializable {
    private static final long serialVersionUID = 708289317982993723L;

    private Date expectBeginDate;
    private Date expectEndDate;

    private Long id;
    /**
     * 项目id
     */
    private Long projectId;
    /**
     * 1、研发
     */
    private Integer type;
    /**
     * 创建者
     */
    private Long createBy;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 状态 1、未开始 2、进行中 3、已完成 4、已关闭 -1已删除
     */
    private Integer status;
    /**
     * 指派给他人
     */
    private Long assginTo;
    /**
     * 指派时间
     */
    private LocalDateTime assignTime;
    /**
     * 完成者
     */
    private Long finishBy;
    /**
     * 完成时间
     */
    private LocalDateTime finishTime;
    /**
     * 关闭者
     */
    private Long closeBy;
    /**
     * 关闭时间
     */
    private LocalDateTime closeTime;
    /**
     * 取消者
     */
    private Long cancelBy;
    /**
     * 取消时间
     */
    private LocalDateTime cancelTime;
    /**
     * 任务名称
     */
    private String name;
    /**
     * 预计小时数
     */
    private Integer expectHour;
    /**
     * 任务描述
     */
    private String describe;
    /**
     * 消耗小时数
     */
    private Integer consumHour;
    /**
     * 剩余小时数
     */
    private Integer surplusHour;
    /**
     * 任务开始时间
     */
    private LocalDateTime beginTime;
    /**
     * 备注
     */
    private String remark;


}