package com.liu.manag.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.io.Serializable;

/**
 * 消息投递日志(MsgLog)实体类
 *
 * @author makejava
 * @since 2020-03-02 17:42:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MsgLog implements Serializable {
    private static final long serialVersionUID = 991247179998342288L;
    /**
    * 消息唯一标识
    */
    private String msgId;
    /**
    * 消息体, json格式化
    */
    private String msg;
    /**
    * 交换机
    */
    private String exchange;
    /**
    * 路由键
    */
    private String routingKey;
    /**
    * 状态: 0投递中 1投递成功 2投递失败 3已消费
    */
    private Integer status;
    /**
    * 重试次数
    */
    private Integer tryCount;
    /**
    * 下一次重试时间
    */
    private LocalDateTime nextTryTime;
    /**
    * 创建时间
    */
    private LocalDateTime createTime;
    /**
    * 更新时间
    */
    private LocalDateTime updateTime;


}