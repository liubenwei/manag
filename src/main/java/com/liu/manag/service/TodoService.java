package com.liu.manag.service;

import com.liu.manag.entity.Todo;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

/**
 * (Todo)表服务接口
 *
 * @author liu
 * @since 2020-03-11 10:09:10
 */
public interface TodoService {

    List<Todo> queryByUserIdAndIds(Long userId,Long[] ids);

    void update(Long userId,Long id,Date date, int status, String describe, int begin, int end);

    void insert(Long userId, Date date, int type, String name, String describe, int begin, int end);

    List<Todo> queryByStatus(Long userId,int status,int page, int size);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Todo queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Todo> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param todo 实例对象
     * @return 实例对象
     */
    Todo insert(Todo todo);

    /**
     * 修改数据
     *
     * @param todo 实例对象
     * @return 实例对象
     */
    Todo update(Todo todo);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long userId,Long id);

}