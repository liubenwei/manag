package com.liu.manag.service;

import com.liu.manag.entity.Message;

import java.time.LocalDateTime;
import java.util.List;

/**
 * (Message)表服务接口
 *
 * @author liu
 * @since 2020-03-19 20:37:30
 */
public interface MessageService {

    List<Message> findByConversationId(String conversationId);

    void addMessage(Long fromId, Long toId, String content, LocalDateTime localDateTime);
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Message queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Message> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param message 实例对象
     * @return 实例对象
     */
    Message insert(Message message);

    /**
     * 修改数据
     *
     * @param message 实例对象
     * @return 实例对象
     */
    Message update(Message message);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}