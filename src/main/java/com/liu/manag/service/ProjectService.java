package com.liu.manag.service;

import com.liu.manag.entity.Project;

import java.util.Date;
import java.util.List;

/**
 * (Project)表服务接口
 *
 * @author liu
 * @since 2020-03-14 08:20:32
 */
public interface ProjectService {

    Long insertProject(String projectName, Date beginDate, Date endDate, Integer usableDays, String describe, Long createBy,String username);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Project queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Project> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param project 实例对象
     * @return 实例对象
     */
    Project insert(Project project);

    /**
     * 修改数据
     *
     * @param project 实例对象
     * @return 实例对象
     */
    Project update(Project project);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}