package com.liu.manag.service;

import com.liu.manag.entity.ManHour;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

/**
 * (ManHour)表服务接口
 *
 * @author liu
 * @since 2020-03-20 10:09:47
 */
public interface ManHourService {

    List<ManHour> getByTaskId(Long taskId);

    void addManHour(Long taskId, Integer consumHour, Integer surplusHour, String remark, Date date,Long userId,String username);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ManHour queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ManHour> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param manHour 实例对象
     * @return 实例对象
     */
    ManHour insert(ManHour manHour);

    /**
     * 修改数据
     *
     * @param manHour 实例对象
     * @return 实例对象
     */
    ManHour update(ManHour manHour);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}