package com.liu.manag.service.impl;

import com.liu.manag.entity.Todo;
import com.liu.manag.dao.TodoDao;
import com.liu.manag.enums.StatusEnum;
import com.liu.manag.service.TodoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * (Todo)表服务实现类
 *
 * @author liu
 * @since 2020-03-11 10:09:10
 */
@Service("todoService")
public class TodoServiceImpl implements TodoService {
    @Resource
    private TodoDao todoDao;

    @Override
    public List<Todo> queryByUserIdAndIds(Long userId, Long[] ids) {
        if (ids == null) {
            return todoDao.queryByUserId(userId);
        }
        return todoDao.queryByIds(userId, ids);
    }

    @Override
    public void update(Long userId, Long id, Date date, int status, String describe, int begin, int end) {
        Todo todo = new Todo();
        todo.setDate(date);
        todo.setDescribe(describe);
        todo.setState(status);
        todo.setBegin(begin);
        todo.setEnd(end);
        todo.setUserId(userId);
        todo.setId(id);
        todoDao.update(todo);
    }

    @Override
    public void insert(Long userId, Date date, int type, String name, String describe, int begin, int end) {
        Todo todo = new Todo();
        todo.setDate(date);
        todo.setBegin(begin);
        todo.setEnd(end);
        todo.setType(type);
        todo.setName(name);
        todo.setDescribe(describe);
        todo.setState(StatusEnum.NOT_BEGIN.getStatus());
        todo.setUserId(userId);
        todoDao.insert(todo);
    }

    @Override
    public List<Todo> queryByStatus(Long userId, int status, int page, int size) {
        return todoDao.queryByStatus(userId, status, page, size);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Todo queryById(Long id) {
        return this.todoDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Todo> queryAllByLimit(int offset, int limit) {
        return this.todoDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param todo 实例对象
     * @return 实例对象
     */
    @Override
    public Todo insert(Todo todo) {
        this.todoDao.insert(todo);
        return todo;
    }

    /**
     * 修改数据
     *
     * @param todo 实例对象
     * @return 实例对象
     */
    @Override
    public Todo update(Todo todo) {
        this.todoDao.update(todo);
        return this.queryById(todo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long userId, Long id) {
        return this.todoDao.deleteById(userId, id) > 0;
    }
}