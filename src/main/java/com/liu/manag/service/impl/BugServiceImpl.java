package com.liu.manag.service.impl;

import com.liu.manag.entity.ActionLog;
import com.liu.manag.entity.Bug;
import com.liu.manag.dao.BugDao;
import com.liu.manag.entity.User;
import com.liu.manag.enums.StatusEnum;
import com.liu.manag.enums.TypeEnum;
import com.liu.manag.service.ActionLogService;
import com.liu.manag.service.BugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * (Bug)表服务实现类
 *
 * @author liu
 * @since 2020-03-19 10:28:08
 */
@Service("bugService")
public class BugServiceImpl implements BugService {
    @Resource
    private BugDao bugDao;

    @Autowired
    private ActionLogService actionLogService;

    @Override
    public void updateBug(Long bugId, String title, String describe, String remark, Long assignTo, Integer bugType, Integer status, Long userId, User user, Long confirmBy) {
        Bug bug = new Bug();
        bug.setId(bugId);
        bug.setTitle(title);
        bug.setRemark(remark);
        bug.setAssignTo(assignTo);
        bug.setAssignTime(LocalDateTime.now());
        bug.setBugType(bugType);
        bug.setStatus(status);
        bug.setConfirmBy(confirmBy);
        bug.setConfirmTime(LocalDateTime.now());
        if (status == StatusEnum.FINISHED.getStatus()) {
            bug.setFinishBy(userId);
            bug.setFinishTime(LocalDateTime.now());
            Bug bug1 = bugDao.queryById(bugId);
            bug.setAssignTo(bug1.getCreateBy());
            bug.setAssignTime(LocalDateTime.now());
            actionLogService.addActionLog(user.getId(), user.getUsername(), "解决了", TypeEnum.BUG.getType(), bugId, title);
        } else {
            actionLogService.addActionLog(user.getId(), user.getUsername(), "编辑了", TypeEnum.BUG.getType(), bugId, title);

        }
        bugDao.updateBug(bug);
        //添加操作日志


    }

    @Override
    public Integer addBug(Long projectId, String version, Long assignTo, Integer bugType, String title, String describe, Long create_by) {
        Bug bug = new Bug();
        bug.setProjectId(projectId);
        bug.setVersion(version);
        bug.setAssignTo(assignTo);
        bug.setAssignTime(LocalDateTime.now());
        bug.setBugType(bugType);
        bug.setTitle(title);
        bug.setDescribe(describe);
        bug.setCreateBy(create_by);
        bug.setCreateTime(LocalDateTime.now());
        bug.setStatus(StatusEnum.NOT_BEGIN.getStatus());
        bugDao.insert(bug);

        //添加日志
        actionLogService.addActionLog(create_by, null, "新建", TypeEnum.BUG.getType(), bug.getId(), bug.getTitle());


        return null;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Bug queryById(Long id) {
        return this.bugDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Bug> queryAllByLimit(int offset, int limit) {
        return this.bugDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param bug 实例对象
     * @return 实例对象
     */
    @Override
    public Bug insert(Bug bug) {
        this.bugDao.insert(bug);
        return bug;
    }

    /**
     * 修改数据
     *
     * @param bug 实例对象
     * @return 实例对象
     */
    @Override
    public Bug update(Bug bug) {
        this.bugDao.update(bug);
        return this.queryById(bug.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id, User user) {
        Bug bug = bugDao.queryById(id);
        //添加日志
        actionLogService.addActionLog(user.getId(), user.getUsername(), "删除了", TypeEnum.BUG.getType(), id, bug.getTitle());

        return this.bugDao.deleteById(id) > 0;
    }

    @Override
    public List<Bug> getBug(Long projectId, Integer status, Long assignTo, Long finishBy, Long createBy, User operator, Integer page, Integer size) {
        Bug bug = new Bug();
        bug.setProjectId(projectId);
        bug.setStatus(status);
        bug.setAssignTo(assignTo);
        bug.setAssignTime(LocalDateTime.now());
        bug.setFinishBy(finishBy);
        bug.setCreateBy(createBy);


        return bugDao.getBug(bug, page, size);
    }
}