package com.liu.manag.service.impl;

import com.liu.manag.entity.ManHour;
import com.liu.manag.dao.ManHourDao;
import com.liu.manag.enums.TypeEnum;
import com.liu.manag.service.ActionLogService;
import com.liu.manag.service.ManHourService;
import com.liu.manag.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * (ManHour)表服务实现类
 *
 * @author liu
 * @since 2020-03-20 10:09:47
 */
@Service("manHourService")
public class ManHourServiceImpl implements ManHourService {
    @Resource
    private ManHourDao manHourDao;

    @Autowired
    private TaskService taskService;

    @Autowired
    private ActionLogService actionLogService;

    @Override
    public List<ManHour> getByTaskId(Long taskId) {
        return manHourDao.queryByTaskId(taskId);
    }

    @Override
    public void addManHour(Long taskId, Integer consumHour, Integer surplusHour, String remark, Date date, Long userId,String username) {
        ManHour manHour = new ManHour();
        manHour.setTaskId(taskId);
        manHour.setMark(remark);
        manHour.setConsumHour(consumHour);
        manHour.setSurplusHour(surplusHour);
        manHour.setDate(date);
        manHour.setCreateBy(userId);
        manHour.setCreateTime(LocalDateTime.now());
        manHourDao.insert(manHour);

        //记录工时

        actionLogService.addActionLog(userId,username,"记录工时,消耗", TypeEnum.TASK.getType(),manHour.getId(),taskService.queryById(taskId).getName());
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ManHour queryById(Long id) {
        return this.manHourDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<ManHour> queryAllByLimit(int offset, int limit) {
        return this.manHourDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param manHour 实例对象
     * @return 实例对象
     */
    @Override
    public ManHour insert(ManHour manHour) {
        this.manHourDao.insert(manHour);
        return manHour;
    }

    /**
     * 修改数据
     *
     * @param manHour 实例对象
     * @return 实例对象
     */
    @Override
    public ManHour update(ManHour manHour) {
        this.manHourDao.update(manHour);
        return this.queryById(manHour.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.manHourDao.deleteById(id) > 0;
    }
}