package com.liu.manag.service.impl;

import com.liu.manag.dao.ActionLogDao;
import com.liu.manag.entity.ActionLog;
import com.liu.manag.entity.Task;
import com.liu.manag.dao.TaskDao;
import com.liu.manag.enums.StatusEnum;
import com.liu.manag.enums.TypeEnum;
import com.liu.manag.service.ActionLogService;
import com.liu.manag.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * (Task)表服务实现类
 *
 * @author liu
 * @since 2020-03-14 08:20:51
 */
@Service("taskService")
public class TaskServiceImpl implements TaskService {
    @Resource
    private TaskDao taskDao;

    @Autowired
    private ActionLogService actionLogService;

    @Autowired
    private ActionLogDao actionLogDao;

    @Override
    public List<Task> getByProjecId(Long proejctId,Integer status, Long assignTo, Long finishBy,Long createBy, int page, int size) {

        return taskDao.getByPorjectId(proejctId,status, assignTo, finishBy, createBy, page, size);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addTask(Long projectId, Integer taskType, Long assignTo, String name, String describe, Date beginDate, Date endDate, Integer expectHour, Long createBy, String username) {
        Task task = new Task();
        task.setProjectId(projectId);
        task.setType(taskType);
        task.setAssginTo(assignTo);
        task.setAssignTime(LocalDateTime.now());
        task.setName(name);
        task.setExpectBeginDate(beginDate);
        task.setExpectEndDate(endDate);
        task.setDescribe(describe);
        task.setExpectHour(expectHour);
        task.setCreateBy(createBy);
        task.setCreateTime(LocalDateTime.now());
        //添加任务
        taskDao.insert(task);

        //添加操作日志
        ActionLog actionLog = new ActionLog();
        actionLog.setUserId(createBy);
        actionLog.setUsername(username);
        actionLog.setType(TypeEnum.TASK.getType());
        actionLog.setEntityId(task.getId());
        actionLog.setEntityName(task.getName());
        actionLog.setAction("新建");
        actionLog.setActionTime(task.getAssignTime());
        actionLogDao.insertActionLog(actionLog);

    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Task queryById(Long id) {
        return this.taskDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Task> queryAllByLimit(int offset, int limit) {
        return this.taskDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param task 实例对象
     * @return 实例对象
     */
    @Override
    public Task insert(Task task) {
        this.taskDao.insert(task);
        return task;
    }

    @Override
    public void updateTask(Long taskId,Integer status, String name, String describe, String remark, Long assignTo, Integer taskType, Long userId, String username) {
        Task task = new Task();
        task.setName(name);
        task.setDescribe(describe);
        task.setAssginTo(assignTo);
        task.setAssignTime(LocalDateTime.now());
        task.setType(taskType);
        task.setRemark(remark);
        task.setId(taskId);
        task.setStatus(status);

        if(status == StatusEnum.FINISHED.getStatus()){
            Task task1 = taskDao.queryById(taskId);
            task.setAssginTo(task1.getCreateBy());
            task.setFinishBy(userId);
            task.setFinishTime(LocalDateTime.now());
        }
        taskDao.update(task);



        //添加操作日志

        actionLogService.addActionLog(userId, username, "编辑了", TypeEnum.TASK.getType(), task.getId(), task.getName());

    }

    /**
     * 修改数据
     *
     * @param task 实例对象
     * @return 实例对象
     */
    @Override
    public Task update(Task task) {
        this.taskDao.update(task);
        return this.queryById(task.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.taskDao.deleteById(id) > 0;
    }
}