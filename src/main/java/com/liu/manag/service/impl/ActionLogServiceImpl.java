package com.liu.manag.service.impl;

import com.liu.manag.entity.ActionLog;
import com.liu.manag.dao.ActionLogDao;
import com.liu.manag.service.ActionLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * (ActionLog)表服务实现类
 *
 * @author liu
 * @since 2020-03-16 22:00:27
 */
@Service("actionLogService")
public class ActionLogServiceImpl implements ActionLogService {
    @Resource
    private ActionLogDao actionLogDao;

    @Override
    public void addActionLog(Long userId, String username, String action, int type, Long entityId,String entityName) {
        ActionLog actionLog = new ActionLog();
        actionLog.setUserId(userId);
        actionLog.setAction(action);
        actionLog.setActionTime(LocalDateTime.now());
        actionLog.setType(type);
        actionLog.setEntityId(entityId);
        actionLog.setEntityName(entityName);
        actionLogDao.insertActionLog(actionLog);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public ActionLog queryById(Long id) {
        return this.actionLogDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<ActionLog> queryAllByLimit(int offset, int limit) {
        return this.actionLogDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param actionLog 实例对象
     * @return 实例对象
     */
    @Override
    public ActionLog insert(ActionLog actionLog) {
        this.actionLogDao.insert(actionLog);
        return actionLog;
    }

    /**
     * 修改数据
     *
     * @param actionLog 实例对象
     * @return 实例对象
     */
    @Override
    public ActionLog update(ActionLog actionLog) {
        this.actionLogDao.update(actionLog);
        return this.queryById(actionLog.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.actionLogDao.deleteById(id) > 0;
    }
}