package com.liu.manag.service.impl;

import com.alibaba.fastjson.JSON;
import com.liu.manag.config.RabbitConfig;
import com.liu.manag.dao.MsgLogDao;
import com.liu.manag.entity.MsgLog;
import com.liu.manag.mq.MessageHelper;
import com.liu.manag.pojo.Mail;
import com.liu.manag.service.RabbitmqService;
import com.liu.manag.utils.StringRedisClient;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class RabbitmqServiceImpl implements RabbitmqService {

    @Autowired
    private StringRedisClient stringRedisClient;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private MsgLogDao msgLogDao;

    @Override
    public void send(Mail mail) {
        MsgLog msgLog =new MsgLog();
        msgLog.setMsgId(UUID.randomUUID().toString());
        msgLog.setMsg(JSON.toJSONString(mail));
        msgLog.setExchange(RabbitConfig.MAIL_EXCHANGE_NAME);
        msgLog.setRoutingKey(RabbitConfig.MAIL_ROUTING_KEY_NAME);
        msgLog.setStatus(0);
        msgLog.setTryCount(0);
        msgLogDao.insert(msgLog);
        mail.setMsgId(msgLog.getMsgId());

        //把验证码存入redis
        stringRedisClient.setEx(mail.getTo(),msgLog.getMsgId(),60*5, TimeUnit.SECONDS);
        CorrelationData correlationData = new CorrelationData(msgLog.getMsgId());
        rabbitTemplate.convertAndSend(RabbitConfig.MAIL_EXCHANGE_NAME,RabbitConfig.MAIL_ROUTING_KEY_NAME,
                MessageHelper.objectToMessage(mail),correlationData);

    }
}

