package com.liu.manag.service.impl;

import com.liu.manag.config.UploadConfiguration;
import com.liu.manag.entity.File;
import com.liu.manag.dao.FileDao;
import com.liu.manag.entity.User;
import com.liu.manag.enums.FileTypeEnum;
import com.liu.manag.service.FileService;
import com.liu.manag.utils.FileUtil;
import com.liu.manag.utils.Result;
import com.liu.manag.utils.UploadUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * (File)表服务实现类
 *
 * @author liu
 * @since 2020-03-19 11:51:00
 */
@Service("fileService")
public class FileServiceImpl implements FileService {
    @Resource
    private FileDao fileDao;

    @Override
    public void addFolder(Long parentId, String folderName,User operator) {
        File file =new File();
        file.setParentId(parentId);
        file.setFileName(folderName);
        file.setCreateTime(LocalDateTime.now());
        file.setCreateBy(operator.getId());
        file.setFileType(FileTypeEnum.FOLDER.toString());
        file.setIsShare(1);
        fileDao.insert(file);
    }

    @Override
    public Result<List<File>> findByFolderId(Long folderId, User operator) {
        List<File> files = fileDao.findByFolderId(folderId,operator.getId());

        return null;
    }

    @Override
    public void upload(String name, String md5, Long parentId, MultipartFile file, Long userId) throws IOException {
        String path = UploadConfiguration.path + FileUtil.getUUIDFileName(file);
        FileUtil.write(path,file.getInputStream());
        File file1= new File();
        file1.setMd5(md5);
        file1.setCreateBy(userId);
        file1.setCreateTime(LocalDateTime.now());
        file1.setRealPath(path);
        file1.setIsShare(2);
        file1.setFileType(FileTypeEnum.PDF.toString());
        fileDao.insert(file1);
    }

    @Override
    public void uploadWithBlock(String name, String md5, Long size, Integer chunks, Integer chunk, MultipartFile file) throws IOException {
        String fileName = UploadUtil.getFileName(md5,chunks) + FileUtil.getFileSuffix(name);
        FileUtil.writeWithBlock(UploadConfiguration.path + fileName,size,file.getInputStream(),file.getSize(),chunks,chunk);

        UploadUtil.addChunk(md5,chunk);
        if (UploadUtil.isUploaded(md5)){
            UploadUtil.removeKey(md5);
            File file1= new File();
            file1.setMd5(md5);
            file1.setCreateTime(LocalDateTime.now());
            file1.setFileSize(file.getSize());
            file1.setFileName(name);
            file1.setIsShare(2);
            file1.setFileType(FileTypeEnum.PDF.toString());
            file1.setRealPath(UploadConfiguration.path+fileName);
//            file1.setCreateBy(userId);
//            file1.setParentId(parentId);
            fileDao.insert(file1);
        }
    }

    @Override
    public boolean checkMd5(String md5) {
        File file = fileDao.getByMd5(md5);

        return file != null;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public File queryById(Long id) {
        return this.fileDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<File> queryAllByLimit(int offset, int limit) {
        return this.fileDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param file 实例对象
     * @return 实例对象
     */
    @Override
    public File insert(File file) {
        this.fileDao.insert(file);
        return file;
    }

    /**
     * 修改数据
     *
     * @param file 实例对象
     * @return 实例对象
     */
    @Override
    public File update(File file) {
        this.fileDao.update(file);
        return this.queryById(file.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.fileDao.deleteById(id) > 0;
    }
}