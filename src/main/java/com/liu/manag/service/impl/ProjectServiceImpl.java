package com.liu.manag.service.impl;

import com.liu.manag.entity.Project;
import com.liu.manag.dao.ProjectDao;
import com.liu.manag.enums.TypeEnum;
import com.liu.manag.service.ActionLogService;
import com.liu.manag.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

/**
 * (Project)表服务实现类
 *
 * @author liu
 * @since 2020-03-14 08:20:32
 */
@Service("projectService")
public class ProjectServiceImpl implements ProjectService {
    @Resource
    private ProjectDao projectDao;

    @Autowired
    private ActionLogService actionLogService;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long insertProject(String projectName, Date beginDate, Date endDate, Integer usableDays, String describe, Long createBy, String username) {

        Project project = new Project();
        project.setName(projectName);
        project.setBeginDate(beginDate);
        project.setEndDate(endDate);
        project.setUsableDays(usableDays);
        project.setDescribe(describe);
        project.setCreateBy(createBy);
        project.setCreateTime(LocalDateTime.now());
        projectDao.insertProject(project);

        //添加操作日志
        actionLogService.addActionLog(createBy, username, "新建", TypeEnum.PROJECT.getType(), project.getId(), projectName);

        return project.getId();


    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Project queryById(Long id) {
        return this.projectDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Project> queryAllByLimit(int offset, int limit) {
        return this.projectDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param project 实例对象
     * @return 实例对象
     */
    @Override
    public Project insert(Project project) {
        this.projectDao.insert(project);
        return project;
    }

    /**
     * 修改数据
     *
     * @param project 实例对象
     * @return 实例对象
     */
    @Override
    public Project update(Project project) {
        this.projectDao.update(project);
        return this.queryById(project.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Long id) {
        return this.projectDao.deleteById(id) > 0;
    }
}