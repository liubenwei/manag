package com.liu.manag.service;

import com.liu.manag.entity.ActionLog;
import java.util.List;

/**
 * (ActionLog)表服务接口
 *
 * @author liu
 * @since 2020-03-16 22:00:27
 */
public interface ActionLogService {

    void addActionLog(Long userId,String username,String action,int type,Long entityId,String entityName);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    ActionLog queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ActionLog> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param actionLog 实例对象
     * @return 实例对象
     */
    ActionLog insert(ActionLog actionLog);

    /**
     * 修改数据
     *
     * @param actionLog 实例对象
     * @return 实例对象
     */
    ActionLog update(ActionLog actionLog);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}