package com.liu.manag.service;

import com.liu.manag.entity.File;
import com.liu.manag.entity.User;
import com.liu.manag.utils.Result;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * (File)表服务接口
 *
 * @author liu
 * @since 2020-03-19 11:51:00
 */
public interface FileService {

    void addFolder(Long parentId, String folderName, User operator);

    Result<List<File>> findByFolderId(Long folderId, User operator);

    void upload(String name,String md5,Long parentId,MultipartFile file, Long userId) throws IOException;
    void uploadWithBlock( String name, String md5, Long size, Integer chunks, Integer chunk, MultipartFile file) throws IOException;


    boolean checkMd5(String md5);
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    File queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<File> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param file 实例对象
     * @return 实例对象
     */
    File insert(File file);

    /**
     * 修改数据
     *
     * @param file 实例对象
     * @return 实例对象
     */
    File update(File file);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}