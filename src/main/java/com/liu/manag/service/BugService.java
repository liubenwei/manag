package com.liu.manag.service;

import com.liu.manag.entity.Bug;
import com.liu.manag.entity.User;

import java.util.List;

/**
 * (Bug)表服务接口
 *
 * @author liu
 * @since 2020-03-19 10:28:08
 */
public interface BugService {


    void updateBug(Long bugId,String bugName,String describe,String remark,Long assignTo,Integer taskType,Integer status,Long userId,User user,Long confirmBy);

    Integer addBug(Long projectId,String version,Long assignTo,Integer bugType,String title,String describe,Long create_by);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Bug queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Bug> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param bug 实例对象
     * @return 实例对象
     */
    Bug insert(Bug bug);

    /**
     * 修改数据
     *
     * @param bug 实例对象
     * @return 实例对象
     */
    Bug update(Bug bug);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id,User user);

    List<Bug> getBug(Long projectId, Integer status, Long assignTo, Long finishBy, Long createBy, User operator,Integer page, Integer size);
}