package com.liu.manag.service;

import com.liu.manag.pojo.Mail;

public interface RabbitmqService {

    public void send(Mail mail);
}
