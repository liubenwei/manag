package com.liu.manag.service;

import com.liu.manag.entity.Task;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Date;
import java.util.List;

/**
 * (Task)表服务接口
 *
 * @author liu
 * @since 2020-03-14 08:20:51
 */
public interface TaskService {


    List<Task> getByProjecId(Long proejctId,Integer status, Long assignTo,Long finishBy,Long createBy,int page, int size);
    void addTask(Long projectId, Integer taskType, Long assignTo, String name,
                    String describe, Date beginDate, Date endDate, Integer expectHour,Long createBy,String username);

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Task queryById(Long id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Task> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param task 实例对象
     * @return 实例对象
     */
    Task insert(Task task);

    void updateTask(Long taskId,Integer status,String name, String describe, String remark, Long assignTo, Integer taskType,
                Long userId,String username);

    /**
     * 修改数据
     *
     * @param task 实例对象
     * @return 实例对象
     */
    Task update(Task task);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}