package com.liu.manag.security;

import com.liu.manag.dao.UserDao;
import com.liu.manag.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Service
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    public User loadUserByEmailAndPassword(String email, String password) {

        return userDao.queryByEmailAndPassword(email, password);
    }

    public User loadUserByEmailAndSms(String email, String code) {
        return userDao.queryByEmail(email);

    }

    public User loadUserByUsernameAndPassword(String username, String password) {
        return userDao.queryByUsernameAndPassword(username, password);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {


        return new org.springframework.security.core.userdetails.User(
                "sadf", "sadf", null);
    }
}
