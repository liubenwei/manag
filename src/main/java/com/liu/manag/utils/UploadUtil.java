package com.liu.manag.utils;

import java.util.HashMap;
import java.util.Map;

public class UploadUtil {

    /**
     * 内部类记录分块上传文件信息
     */
    private static class Value {
        String name;
        boolean[] status;

        Value(int n) {
            this.name = FileUtil.generateFileName();
            this.status = new boolean[n];
        }
    }


    private static Map<String, Value> chunkMap = new HashMap<>();

    /**
     * 判断文件所有分块是否已经上传
     *
     * @param key
     * @return
     */
    public static boolean isUploaded(String key) {
        if (isExist(key)){
            for(boolean b : chunkMap.get(key).status){
                if(!b){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    private static boolean isExist(String key) {
        return chunkMap.containsKey(key);
    }

    public static void addChunk(String key, int chunk) {
        chunkMap.get(key).status[chunk] = true;
    }

    public static void removeKey(String key) {
        if (isExist(key)) {
            chunkMap.remove(key);
        }
    }

    public static String getFileName(String key, int chunks) {
        if (!isExist(key)) {
            synchronized (UploadUtil.class) {
                if (!isExist(key)) {
                    chunkMap.put(key, new Value(chunks));
                }
            }
        }
        return chunkMap.get(key).name;
    }


}
