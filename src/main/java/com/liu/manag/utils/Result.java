package com.liu.manag.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
@AllArgsConstructor
@Data
public class Result<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private int code;
    private int status;
    private String message;
    private T data;

    private Object extra;

    public Result(T data){
        code = 200;
        status = 200;
        message = "请求成功";
        this.data = data;

    }

    public Result() {
    }

    public static <T>  Result<T> sucess(String message){
        return new Result<>(200,200,message);
    }

    public Result(int resultCode,int status, String message) {
        this.status = status;
        this.code = resultCode;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
