package com.liu.manag.utils;

import com.liu.manag.pojo.Mail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MailUtil {
    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.from}")
    private String from;


    /**
     * 发送简单邮件
     *
     * @param mail
     * @return
     */
    public boolean send(Mail mail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(mail.getTo());
        message.setSubject(mail.getTitle());
        message.setText(mail.getContent());

        try {
            javaMailSender.send(message);
            log.info("邮件发送成功");
            return true;
        }catch (MailException e){
            log.error("邮件发送失, to:{}, title:{}",mail.getTo(),mail.getTitle(),e);
            return false;
        }

    }
}
